﻿using GART.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UNAM360
{
    public class UNAM360Item : ARItem
    {
        private string urlicon;
        private string edificio;
        //private string description;
        private int id;

        public string Urlicon
        {
            get
            {
                return urlicon;
            }
            set
            {
                if (urlicon != value)
                {
                    urlicon = value;
                    NotifyPropertyChanged(() => Urlicon);
                }
            }
        }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                if (id != value)
                {
                    id = value;
                    NotifyPropertyChanged(() => id);
                }
            }

        }

        public string Edificio
        {
            get
            {
                return edificio;
            }
            set
            {
                if (edificio != value)
                {
                    edificio = value;
                    NotifyPropertyChanged(() => Edificio);
                }
            }
        }

    }
}
