﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Parse;
using System.IO.IsolatedStorage;

namespace UNAM360
{
    public partial class MainPage : PhoneApplicationPage
    {
        #region Variables

        public static string id;
        public static string identifica;
        public static string category;
        List<Lugar> ListaCulturales = new List<Lugar>();
        List<Lugar> ListaDeportes = new List<Lugar>();
        List<Lugar> ListaEstablecimientos = new List<Lugar>();
        List<Lugar> ListaFacultades = new List<Lugar>();
        List<Lugar> ListaInstitutos = new List<Lugar>();
        List<Lugar> ListaComunidades = new List<Lugar>();
        IsolatedStorageSettings settings;

        #endregion
        public MainPage()
        {
            InitializeComponent();
            getCategories();
        }

        #region Metodos

        async void getCategories()
        {
            settings = IsolatedStorageSettings.ApplicationSettings;

            try
            {
                if (settings.Contains("Culturales"))
                {
                    //Getting data from Storage

                    List<Lugar> getSavedListCulturales = new List<Lugar>();
                    getSavedListCulturales = (List<Lugar>)settings["Culturales"];//Here is the data
                    ListaCulturales = getSavedListCulturales;
                }

                else
                {
                    //Getting data from Parse if doesn´t exist on de device

                    var query = ParseObject.GetQuery("Lugares")
                     .WhereEqualTo("Categoria", "Culturales");
                    IEnumerable<ParseObject> results = await query.FindAsync();
                    Culturales.ItemsSource = results;
                    ListaCulturales = (Culturales.ItemsSource as IEnumerable<Lugar>).ToList();

                    //Saving Data to Device

                    settings.Add("Culturales", ListaCulturales);  
                    settings.Save();
                }

            }

            catch(Exception e)
            {
                Console.WriteLine("Ocurio un Error al recuperar Datos:" + e.Message);
            }
        }

        #endregion

        #region Eventos

        private void Culturales_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Culturales.SelectedIndex == -1)
                return;

            // Navigate to the new page
            identifica = ListaCulturales.ElementAt(Culturales.SelectedIndex).identificador.ToString();
            category = ListaCulturales.ElementAt(Culturales.SelectedIndex).categoria.ToString();

            NavigationService.Navigate(new Uri("/PivotPage1.xaml?selectedItem=" + identifica, UriKind.Relative));
            // Reset selected index to -1 (no selection)
            Culturales.SelectedIndex = -1;
        }

        /*
        private void Deportes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            if (Deportes.SelectedIndex == -1)
                return;

            // Navigate to the new page
            identifica = ListaDeportes.ElementAt(Deportes.SelectedIndex).identificador.ToString();
            category = ListaDeportes.ElementAt(Deportes.SelectedIndex).categoria.ToString();

            NavigationService.Navigate(new Uri("/PivotPage1.xaml?selectedItem=" + identifica, UriKind.Relative));
            // Reset selected index to -1 (no selection)
            Deportes.SelectedIndex = -1;
            
        }

        private void Establecimientos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            if (Establecimientos.SelectedIndex == -1)
                return;
            
            // Navigate to the new page
            identifica = ListaEstablecimientos.ElementAt(Establecimientos.SelectedIndex).identificador.ToString();
            category = ListaEstablecimientos.ElementAt(Establecimientos.SelectedIndex).categoria.ToString();

            NavigationService.Navigate(new Uri("/PivotPage1.xaml?selectedItem=" + identifica, UriKind.Relative));
            // Reset selected index to -1 (no selection)
            Establecimientos.SelectedIndex = -1;
             
        }

        private void Facultades_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            if (Facultades.SelectedIndex == -1)
                return;
            
            // Navigate to the new page
            identifica = ListaFacultades.ElementAt(Facultades.SelectedIndex).identificador.ToString();
            category = ListaFacultades.ElementAt(Facultades.SelectedIndex).categoria.ToString();

            NavigationService.Navigate(new Uri("/PivotPage1.xaml?selectedItem=" + identifica, UriKind.Relative));
            // Reset selected index to -1 (no selection)
            Facultades.SelectedIndex = -1;
            
        }

        private void Institutos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            if (Institutos.SelectedIndex == -1)
                return;

            // Navigate to the new page
            identifica = ListaInstitutos.ElementAt(Institutos.SelectedIndex).identificador.ToString();
            category = ListaInstitutos.ElementAt(Institutos.SelectedIndex).categoria.ToString();

            NavigationService.Navigate(new Uri("/PivotPage1.xaml?selectedItem=" + identifica, UriKind.Relative));
            // Reset selected index to -1 (no selection)
            Institutos.SelectedIndex = -1;
            
        }

        private void Comunidad_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            if (Comunidad.SelectedIndex == -1)
                return;

            // Navigate to the new page
            identifica = ListaComunidades.ElementAt(Comunidad.SelectedIndex).identificador.ToString();
            category = ListaComunidades.ElementAt(Comunidad.SelectedIndex).categoria.ToString();

            NavigationService.Navigate(new Uri("/PivotPage1.xaml?selectedItem=" + identifica, UriKind.Relative));
            // Reset selected index to -1 (no selection)
            Comunidad.SelectedIndex = -1;
            
        }
        */
        #endregion

    }
}