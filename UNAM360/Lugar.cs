﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UNAM360
{
    public class Lugar
    {
        public string identificador { get; set; }
        public string categoria { get; set; }
        public double latitud { get; set; }
        public double longitud { get; set; }
        public string imagen360 { get; set; }
        public string descripcion { get; set; }
        public string nombre { get; set; }
    }
}
